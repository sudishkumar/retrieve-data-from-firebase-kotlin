package com.example.myfirestore

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore



class MainActivity : AppCompatActivity() {
    lateinit var firestore: FirebaseFirestore
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val showName = findViewById<Button>(R.id.show_data)
        showName.setOnClickListener {
            readData()
        }
    }

    private fun readData(){
        firestore = FirebaseFirestore.getInstance()
        firestore.collection("users")
            .get()
            .addOnSuccessListener {
                Toast.makeText(this, "Your first name is ${it.documents.get(0).data?.get("first_name") }  and last name is ${it.documents.get(0).data?.get("last_name") }", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener{
                it.printStackTrace()
                Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
            }
    }
}